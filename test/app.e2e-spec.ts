import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import {
  ApiKeyMockData,
  correctTestApiKey,
  incorrectTestApiKey,
} from '../src/api-key/api-key.mock';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET): should deny access without an API key', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(ApiKeyMockData.missingBody);
  });

  it('/ (GET): should deny access with an incorrect API key', () => {
    return request(app.getHttpServer())
      .get('/')
      .set('x-api-key', incorrectTestApiKey)
      .expect(401)
      .expect(ApiKeyMockData.incorrectBody);
  });

  it('/ (GET): should allow access with a correct API key', () => {
    // Replace 'correct-key' with the actual correct API key.
    return request(app.getHttpServer())
      .get('/')
      .set('x-api-key', correctTestApiKey)
      .expect(200)
      .expect('Hello World!');
  });
});
