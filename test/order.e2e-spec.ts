import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { correctTestApiKey } from '../src/api-key/api-key.mock';
import { of } from 'rxjs';
import { AxiosResponse } from 'axios';
import { HttpModule, HttpService } from '@nestjs/axios';
import { UsersModule } from '../src/users/users.module';
import { OrderMockData } from '../dist/orders/order.mock';

describe('OrderController (e2e)', () => {
  let app: INestApplication;

  let httpService: HttpService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, HttpModule, UsersModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    httpService = moduleFixture.get<HttpService>(HttpService);
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('/users/:user_id/orders (GET)', () => {
    // Mock the external API response
    const response: AxiosResponse = {
      data: OrderMockData.listErpOrder,
      status: HttpStatus.OK,
      statusText: 'OK',
      headers: {},
      config: undefined,
    };

    jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(response));

    return request(app.getHttpServer())
      .get(`/users/${OrderMockData.userTestId}/orders`)
      .set('x-api-key', correctTestApiKey)
      .expect(HttpStatus.OK)
      .expect(OrderMockData.listOrder);
  });

  it('/users/:user_id/orders/:id (GET)', async () => {
    const response: AxiosResponse = {
      data: OrderMockData.orderErp,
      status: HttpStatus.OK,
      statusText: 'OK',
      headers: {},
      config: undefined,
    };

    jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(response));

    return request(app.getHttpServer())
      .get(
        `/users/${OrderMockData.userTestId}/orders/${OrderMockData.orderErp.id}`,
      )
      .set('x-api-key', correctTestApiKey)
      .expect(HttpStatus.OK)
      .expect(OrderMockData.order);
  });
});
