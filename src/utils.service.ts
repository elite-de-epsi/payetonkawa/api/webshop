import { HttpException, Injectable } from '@nestjs/common';

@Injectable()
export class UtilsService {
  public catchERPerror() {
    return (error) => {
      throw new HttpException(error.message, 400);
    };
  }
}
