import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { correctTestApiKey } from './api-key.mock';

export const apiKeyHeaderName = 'x-api-key';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  constructor(private configService: ConfigService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const apiKey: string = req.headers[apiKeyHeaderName];
    // Check there is an api key
    if (!apiKey) {
      throw new HttpException(
        'You must provide an API key.',
        HttpStatus.UNAUTHORIZED,
      );
    }

    const apiKeyConfig =
      this.configService.get('API_KEY_WEBSHOP') ?? correctTestApiKey;
    // Check if the api key is correct
    if (apiKey !== apiKeyConfig) {
      throw new HttpException(
        'The api key you provided is incorrect.',
        HttpStatus.UNAUTHORIZED,
      );
    }
    return true;
  }
}
