import { HttpStatus } from '@nestjs/common';

export const correctTestApiKey = 'the_correct_api_key';
export const incorrectTestApiKey = 'the_incorrect_api_key';
export const ApiKeyMockData = {
  correctContext: {
    switchToHttp: () => ({
      getRequest: () => {
        return { headers: { 'x-api-key': correctTestApiKey } };
      },
    }),
  },
  missingContext: {
    switchToHttp: () => ({
      getRequest: () => {
        return { headers: {} };
      },
    }),
  },
  incorrectContext: {
    switchToHttp: () => ({
      getRequest: () => {
        return { headers: { 'x-api-key': incorrectTestApiKey } };
      },
    }),
  },
  missingBody: {
    statusCode: HttpStatus.UNAUTHORIZED,
    message: 'You must provide an API key.',
  },
  incorrectBody: {
    statusCode: HttpStatus.UNAUTHORIZED,
    message: 'The api key you provided is incorrect.',
  },
};
