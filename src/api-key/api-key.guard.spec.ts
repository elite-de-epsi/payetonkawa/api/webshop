import { Test, TestingModule } from '@nestjs/testing';
import { ApiKeyGuard } from './api-key.guard';
import { HttpException, HttpStatus } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiKeyMockData, correctTestApiKey } from './api-key.mock';

describe('ApiKeyGuard', () => {
  let guard: ApiKeyGuard;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApiKeyGuard,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    guard = module.get<ApiKeyGuard>(ApiKeyGuard);
    configService = module.get<ConfigService>(ConfigService);
  });

  describe('canActivate', () => {
    it('should return true because it send the correct apiKey', async () => {
      jest.spyOn(configService, 'get').mockReturnValue(correctTestApiKey);

      const result = await guard.canActivate(
        ApiKeyMockData.correctContext as any,
      );
      expect(result).toBe(true);
    });

    it('should throw HttpException if there is no api key', async () => {
      expect.assertions(3);

      jest.spyOn(configService, 'get').mockReturnValue(correctTestApiKey);

      try {
        await guard.canActivate(ApiKeyMockData.missingContext as any);
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(HttpStatus.UNAUTHORIZED);
        expect(e.message).toBe('You must provide an API key.');
      }
    });

    it('should throw HttpException if apiKey is incorrect', async () => {
      expect.assertions(3);

      jest.spyOn(configService, 'get').mockReturnValue(correctTestApiKey);

      try {
        await guard.canActivate(ApiKeyMockData.incorrectContext as any);
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(HttpStatus.UNAUTHORIZED);
        expect(e.message).toBe('The api key you provided is incorrect.');
      }
    });

    it('should take the test api key if the env variable is not set', async () => {
      jest.spyOn(configService, 'get').mockReturnValue(undefined);

      const result = await guard.canActivate(
        ApiKeyMockData.correctContext as any,
      );
      expect(result).toBe(true);
    });
  });
});
