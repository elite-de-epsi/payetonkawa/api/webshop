import { Test, TestingModule } from '@nestjs/testing';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { OrdersModule } from './orders.module';
import { OrderMockData } from './order.mock';
import { lastValueFrom, of } from 'rxjs';

describe('OrdersController', () => {
  let controller: OrdersController;
  let orderService: OrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [OrdersModule],
      controllers: [OrdersController],
      providers: [
        {
          provide: OrdersService,
          useValue: {
            findAllOrders: jest.fn(),
            findOneOrder: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<OrdersController>(OrdersController);
    orderService = module.get<OrdersService>(OrdersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAllOrders', () => {
    it('', async () => {
      jest
        .spyOn(orderService, 'findAllOrders')
        .mockImplementation(() => of(OrderMockData.listOrder));

      const result = await lastValueFrom(
        controller.findAllOrders(OrderMockData.userTestId),
      );

      expect(result).toEqual(OrderMockData.listOrder);
    });
  });

  describe('findOneOrder', () => {
    it('', async () => {
      jest
        .spyOn(orderService, 'findOneOrder')
        .mockImplementation(() => of(OrderMockData.order));

      const result = await lastValueFrom(
        controller.findOneOrder(
          OrderMockData.userTestId,
          OrderMockData.orderTestId,
        ),
      );

      expect(result).toEqual(OrderMockData.order);
    });
  });
});
