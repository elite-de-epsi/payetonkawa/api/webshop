import { Controller, Get, Param } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
import { apiKeyHeaderName } from '../api-key/api-key.guard';

@ApiTags('orders')
@ApiSecurity(apiKeyHeaderName)
@Controller('users')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Get(':user_id/orders')
  findAllOrders(@Param('user_id') userId: string) {
    return this.ordersService.findAllOrders(userId);
  }

  @Get(':user_id/orders/:order_id')
  findOneOrder(
    @Param('user_id') userId: string,
    @Param('order_id') orderId: string,
  ) {
    return this.ordersService.findOneOrder(userId, orderId);
  }
}
