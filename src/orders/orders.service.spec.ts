import { Test, TestingModule } from '@nestjs/testing';
import { OrdersService } from './orders.service';
import { lastValueFrom, of } from 'rxjs';
import { HttpException, HttpStatus } from '@nestjs/common';
import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { OrderMockData } from './order.mock';
import { UtilsService } from '../utils.service';
import { OrdersModule } from './orders.module';

describe('OrdersService', () => {
  let service: OrdersService;
  let httpService: HttpService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), OrdersModule, HttpModule],
      providers: [
        OrdersService,
        UtilsService,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<OrdersService>(OrdersService);
    httpService = module.get<HttpService>(HttpService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAllOrders', () => {
    it('test for find all orders', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() =>
          of({ data: OrderMockData.listErpOrder } as any),
        );
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');
      const result = await lastValueFrom(
        service.findAllOrders(OrderMockData.userTestId),
      );

      expect(result).toEqual(OrderMockData.listOrder);
    });

    it('', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'get').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      try {
        await lastValueFrom(service.findAllOrders(OrderMockData.userTestId));
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });

  describe('findOneOrder', () => {
    it('test for findOneOrder', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => of({ data: OrderMockData.orderErp } as any));
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      const result = await lastValueFrom(
        service.findOneOrder(
          OrderMockData.userTestId,
          OrderMockData.orderTestId,
        ),
      );

      expect(result).toEqual(OrderMockData.order);
    });

    it('', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'get').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      try {
        await lastValueFrom(
          service.findOneOrder(
            OrderMockData.userTestId,
            OrderMockData.orderTestId,
          ),
        );
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });
});
