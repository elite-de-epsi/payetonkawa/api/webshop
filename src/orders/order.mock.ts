const userTestId = '10';
const orderTestId = '10';

export const OrderMockData = {
  userTestId: userTestId,
  orderTestId: orderTestId,

  listErpOrder: [
    {
      id: '10',
      customerId: '10',
    },
    {
      id: '60',
      customerId: '10',
    },
  ],

  listOrder: [
    {
      id: '10',
      customerId: '10',
    },
    {
      id: '60',
      customerId: '10',
    },
  ],

  orderErp: {
    id: '60',
    customerId: '10',
  },

  order: {
    id: '60',
    customerId: '10',
  },
};
