import { Injectable } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { Order } from './dto/order.dto';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { AxiosRequestConfig } from 'axios';
import { UtilsService } from '../utils.service';

@Injectable()
export class OrdersService {
  constructor(
    private readonly httpService: HttpService,
    private configService: ConfigService,
    private utilsService: UtilsService,
  ) {}

  private httpOptions: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json' },
  };

  findAllOrders(user_id: string): Observable<Order[]> {
    return this.httpService
      .get(
        `${this.configService.get('WEB_API')}customers/${user_id}/orders`,
        this.httpOptions,
      )
      .pipe(
        map((response) => response.data),
        map((orders) =>
          orders.map((order) => ({
            id: order.id,
            customerId: order.customerId,
          })),
        ),
        catchError(this.utilsService.catchERPerror()),
      );
  }

  findOneOrder(user_id: string, order_id: string): Observable<Order> {
    return this.httpService
      .get(
        `${this.configService.get(
          'WEB_API',
        )}customers/${user_id}/orders/${order_id}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => response.data),
        map((order) => ({
          id: order.id,
          customerId: order.customerId,
        })),
        catchError(this.utilsService.catchERPerror()),
      );
  }
}
