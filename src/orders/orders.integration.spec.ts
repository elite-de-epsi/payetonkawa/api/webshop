import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { lastValueFrom, of } from 'rxjs';
import { UtilsService } from '../utils.service';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { OrdersModule } from './orders.module';
import { OrderMockData } from './order.mock';

describe('AuthController (Integration)', () => {
  let orderController: OrdersController;
  let orderService: OrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), OrdersModule, HttpModule],
      controllers: [OrdersController],
      providers: [OrdersService, UtilsService],
    }).compile();

    orderController = module.get<OrdersController>(OrdersController);
    orderService = module.get<OrdersService>(OrdersService);
  });

  it('should be defined', () => {
    expect(orderController).toBeDefined();
  });

  describe('findAllOrders', () => {
    it('', async () => {
      const userServiceSpy = jest
        .spyOn(orderService, 'findAllOrders')
        .mockImplementation(() => of(OrderMockData.listOrder));

      await lastValueFrom(
        orderController.findAllOrders(OrderMockData.userTestId),
      );

      expect(userServiceSpy).toHaveBeenCalled();
    });
  });

  describe('findOneOrder', () => {
    it('', async () => {
      const userServiceSpy = jest
        .spyOn(orderService, 'findOneOrder')
        .mockImplementation(() => of(OrderMockData.order));

      await lastValueFrom(
        orderController.findOneOrder(
          OrderMockData.userTestId,
          OrderMockData.orderTestId,
        ),
      );

      expect(userServiceSpy).toHaveBeenCalledWith(
        OrderMockData.userTestId,
        OrderMockData.orderTestId,
      );
    });
  });
});
