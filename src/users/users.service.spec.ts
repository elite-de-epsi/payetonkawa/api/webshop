import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HttpModule, HttpService } from '@nestjs/axios';
import { UtilsService } from '../utils.service';
import { lastValueFrom, of } from 'rxjs';
import { UserMockData } from './user.mock';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UsersModule } from './users.module';

describe('UsersService', () => {
  let service: UsersService;
  let httpService: HttpService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), UsersModule, HttpModule],
      providers: [
        UsersService,
        UtilsService,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    httpService = module.get<HttpService>(HttpService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('test for findAll', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() =>
          of({ data: UserMockData.listErpUser } as any),
        );
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      const result = await lastValueFrom(service.findAll());

      expect(result).toEqual(UserMockData.listUser);
    });

    it('', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'get').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      try {
        await lastValueFrom(service.findAll());
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });

  describe('findOne', () => {
    it('test for findOne', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => of({ data: UserMockData.userErp } as any));
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      const result = await lastValueFrom(
        service.findOne(UserMockData.userTestId),
      );

      expect(result).toEqual(UserMockData.user);
    });

    it('', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'get').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      try {
        await lastValueFrom(service.findOne(UserMockData.userTestId));
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });
});
