import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { User } from './dto/users.dto';
import { ConfigService } from '@nestjs/config';
import { UtilsService } from '../utils.service';
import { AxiosRequestConfig } from 'axios';

@Injectable()
export class UsersService {
  constructor(
    private readonly httpService: HttpService,
    private configService: ConfigService,
    private utilsService: UtilsService,
  ) {}

  private httpOptions: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json' },
  };

  findAll(): Observable<User[]> {
    return this.httpService
      .get(`${this.configService.get('WEB_API')}customers`, this.httpOptions)
      .pipe(
        map((response) => response.data),
        map((users) =>
          users.map((user) => ({
            id: user.id,
            name: user.name,
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName,
            postalCode: user.address.postalCode,
            city: user.address.city,
            company: user.company.companyName,
          })),
        ),
        catchError(this.utilsService.catchERPerror()),
      );
  }

  findOne(user_id: string): Observable<User> {
    return this.httpService
      .get(
        `${this.configService.get('WEB_API')}customers/${user_id}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => response.data),
        map((user) => ({
          id: user.id,
          name: user.name,
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          postalCode: user.address.postalCode,
          city: user.address.city,
          company: user.company.companyName,
        })),
        catchError(this.utilsService.catchERPerror()),
      );
  }
}
