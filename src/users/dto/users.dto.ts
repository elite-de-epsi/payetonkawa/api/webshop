export interface User {
  id: string;
  name: string;
  username: string;
  firstName: string;
  lastName: string;
  postalCode: string;
  city: string;
  company: string;
}
