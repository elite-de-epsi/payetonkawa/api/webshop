import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UsersModule } from './users.module';
import { lastValueFrom, of } from 'rxjs';
import { UserMockData } from './user.mock';

describe('UsersController', () => {
  let controller: UsersController;
  let userService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UsersModule],
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            findAll: jest.fn(),
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    userService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('', async () => {
      jest
        .spyOn(userService, 'findAll')
        .mockImplementation(() => of(UserMockData.listUser));

      const result = await lastValueFrom(controller.findAll());

      expect(result).toEqual(UserMockData.listUser);
    });
  });

  describe('findOne', () => {
    it('', async () => {
      jest
        .spyOn(userService, 'findOne')
        .mockImplementation(() => of(UserMockData.user));

      const result = await lastValueFrom(
        controller.findOne(UserMockData.userTestId),
      );

      expect(result).toEqual(UserMockData.user);
    });
  });
});
