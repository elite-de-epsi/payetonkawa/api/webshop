import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { lastValueFrom, of } from 'rxjs';
import { UtilsService } from '../utils.service';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UsersModule } from './users.module';
import { UserMockData } from './user.mock';

describe('AuthController (Integration)', () => {
  let userController: UsersController;
  let userService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), UsersModule, HttpModule],
      controllers: [UsersController],
      providers: [UsersService, UtilsService],
    }).compile();

    userController = module.get<UsersController>(UsersController);
    userService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(userController).toBeDefined();
  });

  describe('findAll', () => {
    it('', async () => {
      const userServiceSpy = jest
        .spyOn(userService, 'findAll')
        .mockImplementation(() => of(UserMockData.listUser));

      await lastValueFrom(userController.findAll());

      expect(userServiceSpy).toHaveBeenCalled();
    });
  });

  describe('findOne', () => {
    it('', async () => {
      const userServiceSpy = jest
        .spyOn(userService, 'findOne')
        .mockImplementation(() => of(UserMockData.user));

      await lastValueFrom(userController.findOne(UserMockData.userTestId));

      expect(userServiceSpy).toHaveBeenCalledWith(UserMockData.userTestId);
    });
  });
});
