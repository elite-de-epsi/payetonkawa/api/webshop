const userTestId = '10';

export const UserMockData = {
  userTestId: userTestId,
  listErpUser: [
    {
      createdAt: '2023-02-19T15:04:34.584Z',
      name: 'Lela Dickinson',
      username: 'Sienna30',
      firstName: 'Lauren',
      lastName: 'Howell',
      address: { postalCode: '85106-9637', city: 'Adamland' },
      profile: { firstName: 'Wayne', lastName: 'Treutel' },
      company: { companyName: 'Smitham - Balistreri' },
      id: userTestId,
      orders: [
        { createdAt: '2023-02-20T13:12:43.431Z', id: '10', customerId: '10' },
        { createdAt: '2023-02-20T02:22:45.643Z', id: '60', customerId: '10' },
      ],
    },
    {
      createdAt: '2023-02-19T15:58:18.070Z',
      name: 'Stanley Vandervort',
      username: 'Howard.Ullrich10',
      firstName: 'Fanny',
      lastName: 'Davis',
      address: { postalCode: '40340-9198', city: 'Darylmouth' },
      profile: { firstName: 'Teagan', lastName: 'Boyle' },
      company: { companyName: 'Deckow Group' },
      id: '11',
      orders: [
        { createdAt: '2023-02-19T18:12:16.457Z', id: '11', customerId: '11' },
        { createdAt: '2023-02-19T20:58:11.238Z', id: '61', customerId: '11' },
      ],
    },
  ],

  listUser: [
    {
      name: 'Lela Dickinson',
      username: 'Sienna30',
      firstName: 'Lauren',
      lastName: 'Howell',
      postalCode: '85106-9637',
      city: 'Adamland',
      company: 'Smitham - Balistreri',
      id: '10',
    },
    {
      name: 'Stanley Vandervort',
      username: 'Howard.Ullrich10',
      firstName: 'Fanny',
      lastName: 'Davis',
      postalCode: '40340-9198',
      city: 'Darylmouth',
      company: 'Deckow Group',
      id: '11',
    },
  ],

  userErp: {
    createdAt: '2023-02-19T15:58:18.070Z',
    name: 'Stanley Vandervort',
    username: 'Howard.Ullrich10',
    firstName: 'Fanny',
    lastName: 'Davis',
    address: { postalCode: '40340-9198', city: 'Darylmouth' },
    profile: { firstName: 'Teagan', lastName: 'Boyle' },
    company: { companyName: 'Deckow Group' },
    id: '11',
    orders: [
      { createdAt: '2023-02-19T18:12:16.457Z', id: '11', customerId: '11' },
      { createdAt: '2023-02-19T20:58:11.238Z', id: '61', customerId: '11' },
    ],
  },

  user: {
    name: 'Stanley Vandervort',
    username: 'Howard.Ullrich10',
    firstName: 'Fanny',
    lastName: 'Davis',
    postalCode: '40340-9198',
    city: 'Darylmouth',
    company: 'Deckow Group',
    id: '11',
  },
};
