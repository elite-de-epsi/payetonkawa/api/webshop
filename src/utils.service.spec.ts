import { UtilsService } from './utils.service';
import { HttpException } from '@nestjs/common';

describe('UtilsService', () => {
  let utilsService: UtilsService;

  beforeEach(() => {
    utilsService = new UtilsService();
  });

  it('should throw an HttpException with the correct message and status code', () => {
    const errorMessage = 'Test error message';
    const statusCode = 400;
    const catchErrorFn = utilsService.catchERPerror();

    try {
      catchErrorFn({ message: errorMessage });
    } catch (error) {
      expect(error).toBeInstanceOf(HttpException);
      expect(error.message).toEqual(errorMessage);
      expect(error.getStatus()).toEqual(statusCode);
    }
  });
});
