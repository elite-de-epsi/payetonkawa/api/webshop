import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
import { apiKeyHeaderName } from './api-key/api-key.guard';

@Controller()
@ApiTags('hello')
@ApiSecurity(apiKeyHeaderName)
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
